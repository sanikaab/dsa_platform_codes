
/*
Code2: Leaders in an Array
Company : PayU, Adobe, Microsoft, Synopsys, Coditas, Hashedln, Betsol
Platform : GFG
Description:

Given an array A of positive integers. Your task is to find the leaders in the
array. An element of an array is a leader if it is greater than or equal to all the
elements to its right side. The rightmost element is always a leader.

Example 1:
Input:
n = 6
A[] = {16,17,4,3,5,2}
Output: 17 5 2
Explanation: The first leader is 17 as it is greater than all the elements to its
right.
Similarly, the next leader is 5. The right most element is always a leader so it is
also included.
Example 2:
Input:
n = 5
A[] = {1,2,3,4,0}
Output: 4 0
Explanation: 0 is the rightmost element and 4 is the only element which is
greater
than all the elements to its right.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(n)
Constraints:
1 <= n <= 107
0 <= Ai <= 107
*/

import java.util.*;

class Solution {
	void leaderElements(int arr[]) {
		System.out.print("The leaders are : ");
		for(int i = 1 ; i < arr.length ; i++) {
			if((i == arr.length - 1) || (arr[i] > arr[i-1] && arr[i] > arr[i+1])) {
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		obj.leaderElements(arr);
	}
}
