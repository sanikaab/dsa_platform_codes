

/*
 Code 1: Majority Element
Company: Flipkart, Accolite, Amazon, Microsoft, D-E-Shaw, Google, Nagarro, Atlassian
Platform : Leetcode - 169, GFG
Fraz’s & striver’s SDE sheet.
Description

Given an array nums of size n, return the majority element.
The majority element is the element that appears more than [n / 2⌋ times. You
may assume that the majority element always exists in the array.
Example 1:
Input: nums = [3,2,3]
Output: 3
Example 2:
Input: nums = [2,2,1,1,1,2,2]
Output: 2
Constraints:
n == nums.length
1 <= n <= 5 * 104
-109 <= nums[i] <= 109
*/

import java.util.*;

class Solution {

	int majorityElement(int arr[]) {
		int mEle = arr[0];
		int mCount = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			int count = 0;
			for(int j = 0 ; j < arr.length ; j++) {
				if(arr[i] == arr[j]) {
					count++;
				}
			}
			if(count > mCount) {
				mCount = count;
				mEle = arr[i];
			}
		}
		return mEle;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = obj.majorityElement(arr);
		System.out.println("Majority element is : " + ret);
	}
}
