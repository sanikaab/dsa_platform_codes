
/*
 Code 1: Game of Life
Company: Amazon, Goldmansachs, Cuemath
Platform: Leetcode- 289, Coding Ninjas
Fraz’s DSA sheet
Description
According to Wikipedia's article: "The Game of Life, also known simply as
Life, is a cellular automaton devised by the British mathematician John
Horton Conway in 1970."
The board is made up of an m x n grid of cells, where each cell has an initial
state: live (represented by a 1) or dead (represented by a 0). Each cell
interacts with its eight neighbors (horizontal, vertical, diagonal) using the
following four rules (taken from the above Wikipedia article):
1. Any live cell with fewer than two live neighbors dies as if caused by
under-population.
2. Any live cell with two or three live neighbors lives on to the next
generation.
3. Any live cell with more than three live neighbors dies, as if by
overpopulation.
4. Any dead cell with exactly three live neighbors becomes a live cell, as if
by reproduction.
The next state is created by applying the above rules simultaneously to every
cell in the current state, where births and deaths occur simultaneously. Given
the current state of the m x n grid board, return the next state.

Example 1:
Input: board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
Output: [[0,0,0],[1,0,1],[0,1,1],[0,1,0]]
Example 2:
Input: board = [[1,1],[1,0]]
Output: [[1,1],[1,1]]
Constraints:
m == board.length
n == board[i].length
1 <= m, n <= 25
board[i][j] is 0 or 1.
*/


import java.util.Arrays;
import java.util.Scanner;

class Solution {
    static int[][] liveAndDead(int[][] board, int m, int n) {
        int[][] nextBoard = new int[m][n];

        // Iterate through each cell in the board
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int liveNeighbors = countLiveNeighbors(board, m, n, i, j);

                // Apply rules of the Game of Life
                if (board[i][j] == 1) {
                    if (liveNeighbors < 2 || liveNeighbors > 3) {
                        nextBoard[i][j] = 0; // Rule 1 and Rule 3
                    } else {
                        nextBoard[i][j] = 1; // Rule 2
                    }
                } else {
                    if (liveNeighbors == 3) {
                        nextBoard[i][j] = 1; // Rule 4
                    } else {
                        nextBoard[i][j] = 0;
                    }
                }
            }
        }

        return nextBoard;
    }

    // Helper method to count live neighbors for a cell
    private static int countLiveNeighbors(int[][] board, int m, int n, int row, int col) {
        int[][] directions = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};
        int liveNeighbors = 0;

        for (int[] dir : directions) {
            int newRow = row + dir[0];
            int newCol = col + dir[1];
            if (newRow >= 0 && newRow < m && newCol >= 0 && newCol < n && board[newRow][newCol] == 1) {
                liveNeighbors++;
            }
        }

        return liveNeighbors;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the value of m : ");
        int m = sc.nextInt();
        System.out.print("Enter the value of n : ");
        int n = sc.nextInt();
        int[][] board = new int[m][n];

        System.out.println("Enter the elements of the array : ");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = sc.nextInt();
            }
        }

        int[][] nextBoard = liveAndDead(board, m, n);

        System.out.println("Next state of the board:");
        for (int[] row : nextBoard) {
            System.out.println(Arrays.toString(row));
        }
    }
}
