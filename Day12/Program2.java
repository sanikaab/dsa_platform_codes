
/*
 Code 2: Product of maximum in first array and minimum in second
Company: Adobe
Platform: GFG

Description:
Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and
minimum element of the second array.
Example 1:
Input : A[] = {5, 7, 9, 3, 6, 2},
B[] = {1, 2, 6, -1, 0, 9}
Output : -9
Explanation:
The first array is 5 7 9 3 6 2.
The max element among these elements is 9.
The second array is 1 2 6 -1 0 9.
The min element among these elements is -1.
The product of 9 and -1 is 9*-1=-9.
Example 2:
Input : A[] = {0, 0, 0, 0},
B[] = {1, -1, 2}
Output : 0
Expected Time Complexity: O(N + M).
Expected Auxiliary Space: O(1).

Output:
For each test case, output the product of the max element of the first array
and the minimum element of the second array.
Constraints:
1 ≤ N, M ≤ 106
-108 ≤ Ai, Bi ≤ 108
*/

import java.util.*;

class Solution {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the size of 1st array : ");
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];
		System.out.println("Enter the elements of first array : ");
		for(int i = 0 ; i < arr1.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr1[i] = sc.nextInt();
		}
		System.out.println();

		System.out.print("Enter the size of the 2nd array : ");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];
		System.out.println("Enter the elements of second array : ");
		for(int i = 0 ; i < arr2.length ; i++) {
			System.out.print("Enter the elements at " + i + " index : ");
			arr2[i] = sc.nextInt();
		}
		System.out.println();

		// Logic for maximum from first array
		int maxEle = Integer.MIN_VALUE;
		for(int i = 0 ; i < arr1.length ; i++) {
			if(arr1[i] > maxEle) {
				maxEle = arr1[i];
			}
		}

		// Logic for minimum from second array
		int minEle = Integer.MAX_VALUE;
		for(int i = 0 ; i < arr2.length ; i++) {
			if(arr2[i] < minEle) {
				minEle = arr2[i];
			}
		}

		System.out.println("Product = " + (maxEle * minEle));
	}
}
