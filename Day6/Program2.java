
/*
 Code2: Three Great Candidates/ Three Ninja Candidates/
Maximum Product of Three Numbers
Company: Flipkart, Amazon, Snapdeal
Platform: Leetcode- 628, GFG, Coding Ninja
Fraz’s SDE Sheet
Description :
The hiring team aims to find 3 candidates who are great collectively. Each
candidate has his or her ability expressed as an integer. 3 candidates are great
collectively if the product of their abilities is maximum. Given abilities of N candidates in
an array arr[], find the maximum collective ability from the given pool of candidates.
Example 1:
Input:
N = 5
Arr[] = {10, 3, 5, 6, 20}
Output: 1200
Explanation:The multiplication of 10, 6 and 20 is 1200.

Example 2:
Input:
N = 5
Arr[] = {-10, -3, -5, -6, -20}
Output: -90
Explanation:
Multiplication of -3, -5 and -6 is -90.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
3 ≤ N ≤ 107
-105 ≤ Arr[i] ≤ 105
*/

import java.util.*;
import java.util.Arrays;

class Solution {
	int threeMaxSum(int arr[]) {
		Arrays.sort(arr);
		return arr[arr.length -1] * arr[arr.length - 2] * arr[arr.length - 3];
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = obj.threeMaxSum(arr);
		System.out.println("The maximum sum is : " + ret);
	}
}
