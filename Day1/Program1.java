/*
Easy Level : Code 1:Single Number
Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,Avizva, epam, cadence, paytm, atlassian,cultfit+7
Platform: LeetCode - 136
Striver’s SDE Sheet
Description:
Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use only constant extra space.
Example 1:
Input: nums = [2,2,1]
Output: 1
Example 2:
Input: nums = [4,1,2,1,2]
Output: 4
Example 3:
Input: nums = [1]
Output: 1
Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4
Each element in the array appears twice except for one element
which appears only once.
*/

import java.util.*;
//import java.util.Arrays;
class Solution {

	/*  BruteForce Approach
	 
	int uniqueEle(int arr[]) {
		Arrays.sort(arr);
		if(arr.length == 1) {
			return arr[0];
		}
		for(int i = 0 ; i < arr.length ; i++) {
			int count = 0;
			for(int j = 0 ; j < arr.length ; j++) {
				if(arr[i] == arr[j]) {
					count++;
				}
			}
			if(count == 1) {
				return arr[i];
			}
		}
		return -1;
	}
	*/

	int uniqueEle(int arr[]) {
		int ret = arr[0];
		for(int i = 1 ; i < arr.length ; i++) {
			ret = ret ^ arr[i];
		}
		return ret;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter size of the array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = obj.uniqueEle(arr);
		if(ret == -1) {
			System.out.println("-1");
		}else {
			System.out.println("the unique Element is : " + ret);
		}
	}
}

