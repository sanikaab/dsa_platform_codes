
/*
 Code 1:Maximum Subarray
Company:Google, Amazon,Swiggy,Flipkart,Facebook,Infosys
Platform: leetcode-53,Coding Ninjas
Fraz’s DSA Sheet
Description :
Given an integer array nums, find the
subarray
with the largest sum, and return its sum.

Example 1:
Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: The subarray [4,-1,2,1] has the largest sum 6.
Example 2:
Input: nums = [1]
Output: 1
Explanation: The subarray [1] has the largest sum 1.
Example 3:
Input: nums = [5,4,-1,7,8]
Output: 23
Explanation: The subarray [5,4,-1,7,8] has the largest sum 23.

Constraints:
1 <= nums.length <= 105
-104 <= nums[i] <= 104
*/

import java.util.*;

class Solution {
	static int maxSubArray(int arr[]) {
		if(arr.length == 1) {
			return arr[0];
		}
		int sum1 = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			int sum2 = arr[i];
			for(int j = i + 1 ; j < arr.length ; j++) {
				sum2 = sum2 + arr[j];
				if(sum2 > sum1) {
					sum1 = sum2;
				}
			}
		}
		return sum1;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.println("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = maxSubArray(arr);
		System.out.println("Maximum sum is : " + ret);
	}
}
