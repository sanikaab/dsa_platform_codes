
/*
 Code 2: Find the Duplicate Number
Company: Amazon, Microsoft
Platform: Leetcode - 287
Fraz’s and Love Bubbar’s DSA sheet.
Description:
Given an array of integers nums containing n + 1 integers where each integer is in the
range [1, n] inclusive.
There is only one repeated number in nums, return this repeated number.
You must solve the problem without modifying the array nums and uses only constant
extra space.
Example 1:
Input: nums = [1,3,4,2,2]
Output: 2
Example 2:
Input: nums = [3,1,3,4,2]
Output: 3

Constraints:
1 <= n <= 105
nums.length == n + 1
1 <= nums[i] <= n
All the integers in nums appear only once except for precisely one integer which appears
two or more times.
*/

import java.util.*;

class Solution {
	static int findDuplicateNumber(int arr[]) {
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			for(int j = i+1 ; j < arr.length ; j++) {
				if(arr[i] == arr[j]) {
					return arr[i];
				}
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the elements at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int num = findDuplicateNumber(arr);
		System.out.println("Duplicate number is : " + num);
	}
}
