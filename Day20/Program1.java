
/*
 Code 1: Find All Duplicates in an Array
Company: Amazon, Microsoft, OYO Rooms
Platform: Leetcode - 442
Fraz’s DSA sheet.
Description:
Given an integer array nums of length n where all the integers of nums are in the range
[1, n] and each integer appears once or twice, return an array of all the integers that appears
twice.
You must write an algorithm that runs in O(n) time and uses only constant extra space.

Example 1:
Input: nums = [4,3,2,7,8,2,3,1]
Output: [2,3]
Example 2:
Input: nums = [1,1,2]
Output: [1]
Example 3:
Input: nums = [1]
Output: []

Constraints:
n == nums.length
1 <= n <= 105
1 <= nums[i] <= n
*/

import java.util.*;
import java.util.Arrays;

class Solution {
	static void duplicatesInArray(int arr[]) {
		Arrays.sort(arr);
		for(int i = 0 ; i < arr.length-1 ; i++) {
			if(arr[i+1] == arr[i]) {
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.println("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();
		duplicatesInArray(arr);
	}
}
