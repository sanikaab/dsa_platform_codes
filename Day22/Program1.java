
/*
 Code 1: Subarray Sum Equals K (Subarray with given sum-GFG)
Company: Amazon, Facebook, Google, Visa
Platform: Leetcode- 560, GFG, Coding Ninjas
Striver’s DSA sheet
Description
Given an array of integers nums and an integer k, return the total number of subarrays
whose sum equals to k.
A subarray is a contiguous non-empty sequence of elements within an array.
Example 1:
Input: nums = [1,1,1], k = 2
Output: 2
Example 2:
Input: nums = [1,2,3], k = 3
Output: 2

Constraints:
1 <= nums.length <= 2 * 104
-1000 <= nums[i] <= 1000
-107 <= k <= 107
*/

import java.util.*;

class Solution {
	static int subArraySum(int arr[],int k) {
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			int sum = arr[i];
			for(int j = i + 1 ; j < arr.length; j++) {
				if(sum == k) {
					count++;
					break;
				}else {
					sum = sum + arr[j];
				}
			}
		}
		if(arr[arr.length - 1] == k) {
			count++;
		}
		return count;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.print("Enter the element : ");
		int k = sc.nextInt();

		int ret = subArraySum(arr,k);
		System.out.println("No.of Sub-Arrays with the given sum are : " + ret);
	}
}
