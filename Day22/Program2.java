
/*
 Code 2: Maximum of all subarrays of size k
Company: Flipkart, Amazon, Microsoft, Directi, Google
Platform: GFG
Love Bubbar’s DSA sheet.
Description:
Given an array arr[] of size N and an integer K. Find the maximum for each and every
contiguous subarray of size K.

Example 1:
Input:
N = 9, K = 3
arr[] = 1 2 3 1 4 5 2 3 6
Output:
3 3 4 5 5 5 6
Explanation:
1st contiguous subarray = {1 2 3} Max = 3
2nd contiguous subarray = {2 3 1} Max = 3
3rd contiguous subarray = {3 1 4} Max = 4
4th contiguous subarray = {1 4 5} Max = 5
5th contiguous subarray = {4 5 2} Max = 5
6th contiguous subarray = {5 2 3} Max = 5
7th contiguous subarray = {2 3 6} Max = 6

Example 2:
Input:
N = 10, K = 4
arr[] = 8 5 10 7 9 4 15 12 90 13
Output:
10 10 10 15 15 90 90
Explanation:
1st contiguous subarray = {8 5 10 7}, Max = 10
2nd contiguous subarray = {5 10 7 9}, Max = 10
3rd contiguous subarray = {10 7 9 4}, Max = 10
4th contiguous subarray = {7 9 4 15}, Max = 15
5th contiguous subarray = {9 4 15 12},
Max = 15
6th contiguous subarray = {4 15 12 90},
Max = 90
7th contiguous subarray = {15 12 90 13},
Max = 90

Your Task:
You don't need to read input or print anything. Complete the function
max_of_subarrays() which takes the array, N and K as input parameters and returns a
list of integers denoting the maximum of every contiguous subarray of size K.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(k)
Constraints:
1 ≤ N ≤ 105
1 ≤ K ≤ N
0 ≤ arr[i] ≤ 107
*/

import java.util.*;

class Solution {
	static int[] maxSubArrays(int arr[],int k) {
		int arr2[] = new int[arr.length - k + 1];
		int a = 0;
		for(int i = 0 ; i <= arr.length-k ; i++) {
			int max = arr[i];
			for(int j = i + 1 ; j <  k + i ; j++) {
				if(max < arr[j]) {
					max = arr[j];
				}
			}
			System.out.println(max);
			if(a <= arr2.length) {
				arr2[a] = max;
				a++;
			}else {
				break;	
			}
		}
		return arr2;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.println(Arrays.toString(arr));

		System.out.print("Enter the length of the subarray : ");
		int k = sc.nextInt();

		int arr2[] = maxSubArrays(arr,k);

		System.out.println(Arrays.toString(arr2));
	}
}
