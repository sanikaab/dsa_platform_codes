/*
Code 2: Binary Search
Company: Infosys,Oracle,Wipro,Linkedin,Qualcomm,TCS,Cognizant, Accenture
Platform: GFG
Description:
Given a sorted array of size N and an integer K, find the position(0-based
indexing) at which K is present in the array using binary search.
Example 1:
Input:
N = 5
arr[] = {1 2 3 4 5}
K = 4
Output: 3
Explanation: 4 appears at index 3.
Example 2:
Input:
N = 5
arr[] = {11 22 33 44 55}
K = 445
Output: -1
Explanation: 445 is not present.
Expected Time Complexity: O(LogN)
Expected Auxiliary Space: O(LogN) if solving recursively and O(1) otherwise.

Constraints:
1 <= N <= 105
1 <= arr[i] <= 106
1 <= K <= 106 
*/

import java.util.*;
class BinarySearch {

	int binarySearch(int arr[],int search) {

		int start = 0;
		int end = arr.length-1;
		int mid = -1;
		while(start <= end) {

			mid = (start+end)/2;

			if(arr[mid] == search) {

				return mid;
			}
			if(arr[mid] < search){

				start = mid + 1;
			}
			if(arr[mid] > search) {

				end = mid - 1;
			}
			
		}

		return -1;
	}
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter array size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		System.out.println("Enter the element to search:");
		int search = sc.nextInt();

		BinarySearch obj = new BinarySearch();
		int index = obj.binarySearch(arr,search);

		if(index == -1)
			System.out.println("Not found");

		else
			System.out.println("index : " + index);
	}
}
