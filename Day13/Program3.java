/*
Minimum distance between two numbers
Companies : Amazon, Paytm
Platform: GFG

You are given an array a, of n elements. Find the minimum index based distance
between two distinct elements of the array, x and y. Return -1, if either x or y
does not exist in the array.
Example 1:
Input:
N = 4
A[] = {1,2,3,2}
x = 1, y = 2
Output: 1

Explanation: x = 1 and y = 2. There are
two distances between x and y, which are
1 and 3 out of which the least is 1.
Example 2:
Input:
N = 7
A[] = {86,39,90,67,84,66,62}
x = 42, y = 12
Output: -1
Explanation: x = 42 and y = 12. We return
-1 as x and y don't exist in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= a[i], x, y <= 105
x != y
*/

import java.util.Scanner;

class Solution {
    static int minDistance(int[] arr, int x, int y) {
        int minDist = Integer.MAX_VALUE;
        int lastIndexX = -1;
        int lastIndexY = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == x) {
                if (lastIndexY != -1) {
                    minDist = Math.min(minDist, i - lastIndexY);
                }
                lastIndexX = i;
            } else if (arr[i] == y) {
                if (lastIndexX != -1) {
                    minDist = Math.min(minDist, i - lastIndexX);
                }
                lastIndexY = i;
            }
        }

        if (minDist == Integer.MAX_VALUE) {
            return -1;
        }
        return minDist;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of elements in the array (n): ");
        int n = scanner.nextInt();

        int[] arr = new int[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.print("Enter the first number (x): ");
        int x = scanner.nextInt();
        System.out.print("Enter the second number (y): ");
        int y = scanner.nextInt();

        int minDist = minDistance(arr, x, y);
        System.out.println("Output: " + minDist);

	}
}

