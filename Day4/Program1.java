
/*
 Code 1: Max Consecutive Ones

Company: Google, Facebook, Amazon, Microsoft, Apple, Uber, Airbnb, Adobe, Goldman Sachs,
Bloomberg
Platform: Leetcode - 485, Coding Ninja
Striver’s SDE Sheet
Description:
Given a binary array nums, return the maximum number of consecutive 1's in the array.
Example 1:
Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The
maximum number of consecutive 1s is 3.
Example 2:
Input: nums = [1,0,1,1,0,1]
Output: 2

Constraints:
1 <= nums.length <= 105
nums[i] is either 0 or 1.
*/

import java.io.*;

class Solution {
	static int maxCountOne(int arr[]) {
		int maxCount = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			int count2 = 0;
			for(int j = i+1 ; j < arr.length ; j++) {
				if(arr[j] == 1) {
					count2++;
				}else {
					break;
				}
			}			
			if(count2 > maxCount) {
				maxCount = count2;
			}
		}
		return maxCount;
	}
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of the array : ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		
		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println();

		int ret = maxCountOne(arr);
		System.out.println("Maximum number of consecutive 1's is : " + ret);		
	}
}
