
/*
 Code 2: Next Greater Element I

Company: Flipkart, Amazon, Microsoft, MakeMyTrip, Adobe
Platform: Leetcode - 496, GFG
Description:

The next greater element of some element x in an array is the first greater element that
is to the right of x in the same array.
You are given two distinct 0-indexed integer arrays nums1 and nums2, where nums1 is a
subset of nums2.
For each 0 <= i < nums1.length, find the index j such that nums1[i] == nums2[j] and
determine the next greater element of nums2[j] in nums2. If there is no next greater
element, then the answer for this query is -1.
Return an array ans of length nums1.length such that ans[i] is the next greater element
as described above.

Example 1:
Input: nums1 = [4,1,2], nums2 = [1,3,4,2]
Output: [-1,3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 4 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the
answer is -1.
- 1 is underlined in nums2 = [1,3,4,2]. The next greater element is 3.
- 2 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the
answer is -1.

Example 2:
Input: nums1 = [2,4], nums2 = [1,2,3,4]
Output: [3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 2 is underlined in nums2 = [1,2,3,4]. The next greater element is 3.
- 4 is underlined in nums2 = [1,2,3,4]. There is no next greater element, so the
answer is -1.

Constraints:
1 <= nums1.length <= nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 104
All integers in nums1 and nums2 are unique.
All the integers of nums1 also appear in nums2.
*/

import java.util.*;

class Solution {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();
		System.out.print("Enter the size of nums1 : ");
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];
		
		System.out.print("Enter the size of nums2 : ");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];

		for(int i = 0 ; i < arr1.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr1[i] = sc.nextInt();
		}
		System.out.println();
		
		for(int i = 0 ; i < arr2.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr2[i] = sc.nextInt();
		}
		System.out.println();

		for(int i = 0 ; i < arr1.length ; i++) {
			int flag = 0;
			for(int j = 0 ; j < arr2.length ; j++) {
				if((arr2[j] == arr1[i]) && (j == arr2.length -1)) {
					System.out.print(-1);
					break;
				}
				if((arr2[j] == arr1[i]) && (arr2[j+1] > arr1[i])) {
					System.out.print(arr2[j + 1] + " ");
					break;
				}else {
					flag = -1;
				}
			}
			if(flag == 0) {
				System.out.print(-1);
			}
		}

	}
}
