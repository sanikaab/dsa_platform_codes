
/*
 Code 3: Even and Odd

Company: Paytm, Amazon, Microsoft
Platform: GFG
Description:
Given an array arr[] of size N containing equal number of odd and even numbers.
Arrange the numbers in such a way that all the even numbers get the even index and
odd numbers get the odd index.
Note: There are multiple possible solutions, Print any one of them. Also, 0-based
indexing is considered.

Example 1:
Input:
N = 6
arr[] = {3, 6, 12, 1, 5, 8}
Output:
1
Explanation:
6 3 12 1 8 5 is a possible solution.
The output will always be 1 if your
rearrangement is correct.

Example 2:
Input:
N = 4
arr[] = {1, 2, 3, 4}
Output :
1
Your Task:
You don't need to read input or print anything. Your task is to complete the function
reArrange() which takes an integer N and an array arr of size N as input and reArranges
the array in Place without any extra space.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1 ≤ N ≤ 105
1 ≤ arr[i] ≤ 105
*/

import java.util.*;

class Solution {

	boolean reArrange(int arr[]) {
		boolean flag = false;
		for(int i = 0 ; i < arr.length ; i++) {
			for(int j = 0 ; j < arr.length ; j++) {
				if(arr[j] % 2 == 0 && j % 2 == 0) {
					continue;
				}
				if(arr[j] % 2 != 0 && j % 2 != 0) {
					continue;
				}

				if(arr[j] % 2 != 0 && j % 2 == 0) {
					for(int k = j + 1 ; k < arr.length ; k++) {
						if(arr[k] % 2 == 0) {
							int temp = arr[j];
							arr[j] = arr[k];
							arr[k] = temp;
						}
					}
				}

				if(arr[j] % 2 == 0 && j % 2 != 0) {
					for(int k = j + 1 ; j < arr.length ; j++) {
						if(arr[k] % 2 != 0) {
							int temp = arr[j];
							arr[j] = arr[k];
							arr[k] = temp;
						}
					}
				}
			}
		}

		for(int i = 0 ; i < arr.length ; i++) {
			if(arr[i] % 2 == 0 && i % 2 ==0 || arr[i] % 2 != 0 && i % 2 != 0) {
				flag = true;
			}	
		}
		return flag;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		System.out.println("Array befeore Rearrangement : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		
		boolean ret = obj.reArrange(arr);

		System.out.println("Array after Rearrangement : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		if(ret) {
			System.out.println("Rearrange successful : 1");
		}else {
			System.out.println("Rearrange Failed : -1");
		}
	}
}
