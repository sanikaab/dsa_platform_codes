/*Code2: Element with left side smaller and right side greater
Company: Zoho, Amazon, OYO Rooms, Intuit
Platform: GFG
Description :
Given an unsorted array of size N. Find the first element in the array such that all
of its left elements are smaller and all right elements are greater than it.
Note: Left and right side elements can be equal to required elements. And extreme
elements cannot be required.

Example 1:
Input:
N = 4
A[] = {4, 2, 5, 7}
Output:
5
Explanation:
Elements on left of 5 are smaller than 5
and on right of it are greater than 5.

Example 2:
Input:
N = 3
A[] = {11, 9, 12}
Output:
-1

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)

Constraints:
3 <= N <= 106
1 <= A[i] <= 106
*/

import java.util.*;
class ArrayDemo {

	int findElement(int arr[]) {

		int flag = 0;
		for(int i=1;i<arr.length-1;i++) {

			if(arr[i-1] < arr[i] && arr[i+1] > arr[i]) {

				flag = 1;
				return arr[i];
			}
		}
	
		return -1;
	}
}
class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements :");
		for(int i=0;i<arr.length;i++) {
			arr[i] = sc.nextInt();
		}

		int retVal = obj.findElement(arr);
		System.out.println(retVal);
	}
}

