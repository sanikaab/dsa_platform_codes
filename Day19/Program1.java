
/*
 Code 1:Product of Array Except Self
Company: Amazon, Facebook, Microsoft, Goldman Sachs,Qualcomm
Platform: leetcode-238
Fraz’s SDE sheet.
Description:
Given an integer array nums, return an array answer such that answer[i] is
equal to the product of all the elements of nums except nums[i].
The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit
integer.
You must write an algorithm that runs in O(n) time and without using the
division operation.
Example 1:
Input: nums = [1,2,3,4]
Output: [24,12,8,6]
Example 2:
Input: nums = [-1,1,0,-3,3]
Output: [0,0,9,0,0]

Constraints:
2 <= nums.length <= 105
-30 <= nums[i] <= 30
The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.
*/

import java.util.*;

class Solution {
	static int[] productOfArrayExceptSelf(int arr[]) {
		int arr1[] = new int[arr.length];
		for(int i = 0 ; i < arr.length ; i++) {
			int prod = 1;
			for(int j = 0 ; j < arr.length ; j++) {
				if(i == j) {
					continue;
				}else {
					prod = prod * arr[j];
				}
			}
			arr1[i] = prod;
		}
		return arr1;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret[] = productOfArrayExceptSelf(arr);
		System.out.println("Array of product elements is : ");
		for(int i = 0 ; i < ret.length ; i++) {
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}
