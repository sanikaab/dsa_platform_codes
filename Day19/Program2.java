
/*
Code2:Number of Zero-Filled Subarrays
Company: Amazon, Microsoft, OYO Rooms
Platform : GFG,Leetcode-2348
love bubbar’s SDE sheet.
Description:
Given an integer array nums, return the number of subarrays filled with 0.
A subarray is a contiguous non-empty sequence of elements within an
array.
Example 1:
Input: nums = [1,3,0,0,2,0,0,4]
Output: 6
Explanation:
There are 4 occurrences of [0] as a subarray.
There are 2 occurrences of [0,0] as a subarray.
There is no occurrence of a subarray with a size more than 2 filled with 0.
Therefore, we return 6.
Example 2:
Input: nums = [0,0,0,2,0,0]
Output: 9
Explanation:
There are 5 occurrences of [0] as a subarray.
There are 3 occurrences of [0,0] as a subarray.
There is 1 occurrence of [0,0,0] as a subarray.
There is no occurrence of a subarray with a size more than 3 filled with 0.
Therefore, we return 9.
Example 3:
Input: nums = [2,10,2019]
Output: 0
Explanation: There is no subarray filled with 0. Therefore, we return 0.
*/

import java.util.*;

class Solution {
	static int zeroFilledSubArrays(int arr[]) {
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			if(arr[i] == 0) {
				count++;
				for(int j = i+1 ; j < arr.length ; j++) {
					if(arr[j] == 0) {
						count++;
					}else {
						break;
					}
				}
			}
		}
		return count;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the elements at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int count = zeroFilledSubArrays(arr);
		System.out.println("Count of zero filled subarrays : " + count);
	}
}
