
/*
 Code3 : Equilibrium Point
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n non negative numbers. The task is to find the first
equilibrium point in an array. Equilibrium point in an array is an index (or position) such
that the sum of all elements before that index is the same as the sum of elements after
it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: The equilibrium point is at position 3 as the sum of elements
before it (1+3) = sum of elements after it (2+2).
Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation: Since there's only an element hence its only the equilibrium point.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= A[i] <= 109
*/

import java.util.*;

class Solution {
	int equilibriumPoint(int arr[]) {
		int equilibriumPoint = -1;
		for(int i = 0 ; i < arr.length ; i++) {
			int leftSum = 0;
			int rightSum = 0;
			for(int j = 0 ; j < arr.length ; j++) {
				if(i == j) {
					continue;
				}else if(j < i) {
					leftSum = leftSum + arr[j];
				}else {
					rightSum = rightSum + arr[j];
				}
			}
			if(leftSum == rightSum) {
				equilibriumPoint = i;
				return equilibriumPoint + 1;
			}
		}
		return -1;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = obj.equilibriumPoint(arr);
		System.out.println("Equilibrium point is : " + ret);
	}
}
