
/*
Code2 : Equal Left and Right Subarray Sum
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n positive numbers. The task is to find the first index in the
array such that the sum of elements before it equals the sum of elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) =
elements after it (2+2).

Example 2:
Input:

n = 1
A[] = {1}
Output: 1
Explanation: Since it's the only element hence it is the only point.
Expected Time Complexity: O(N)
Expected Space Complexity: O(1)
Constraints:
1 <= n <= 106
1 <= A[i] <= 108
*/

import java.util.*;

class Solution {
	int equalLeftandRightSubArraySum(int arr[]) {
		int index = -1;
		for(int i = 0 ; i < arr.length ; i++) {
			int leftSum = 0;
			int rightSum = 0;
			for(int j = 0 ; j < arr.length ; j++) {
				if(i == j) {
					continue;
				}else if(j < i) {
					leftSum = leftSum + arr[j];
				}else {
					rightSum = rightSum + arr[j];
				}
			}
			if(leftSum == rightSum) {
				index = i;
				return index + 1;
			}
		}
		return index + 1;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = obj.equalLeftandRightSubArraySum(arr);
		System.out.println("The index is : " + ret);
	}
}
