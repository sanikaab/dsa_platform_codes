
/*
 Code 1: Find Pivot Index
Company: Amazon, Adobe
Platform: Leetcode - 724
Given an array of integers nums, calculate the pivot index of this array.
The pivot index is the index where the sum of all the numbers strictly to the left of the
index is equal to the sum of all the numbers strictly to the index's right.
If the index is on the left edge of the array, then the left sum is 0 because there are no
elements to the left. This also applies to the right edge of the array.
Return the leftmost pivot index. If no such index exists, return -1.

Example 1:
Input: nums = [1,7,3,6,5,6]
Output: 3
Explanation:
The pivot index is 3.
Left sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11
Right sum = nums[4] + nums[5] = 5 + 6 = 11
Example 2:
Input: nums = [1,2,3]
Output: -1
Explanation:
There is no index that satisfies the conditions in the problem statement.
Example 3:
Input: nums = [2,1,-1]
Output: 0

Explanation:
The pivot index is 0.
Left sum = 0 (no elements to the left of index 0)
Right sum = nums[1] + nums[2] = 1 + -1 = 0

Constraints:
1 <= nums.length <= 104
-1000 <= nums[i] <= 1000
*/

import java.util.*;

class Solution {
	int pivotIndex(int arr[]) {
		int pivotIndex = -1;
		for(int i = 0 ; i < arr.length ; i++) {
			int rightSum = 0;
			int leftSum = 0;
			for(int j = 0 ; j < arr.length ; j++) {
				if(i == j) {
					continue;
				}else if(i < j) {
					leftSum = leftSum + arr[j];
				}else {
					rightSum = rightSum + arr[j];
				}
			}
			if(leftSum == rightSum) {
				pivotIndex = i;
				return pivotIndex;
			}
		}
		return pivotIndex;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Solution obj = new Solution();

		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int ret = obj.pivotIndex(arr);
		System.out.println("The pivot index is : " + ret);
	}
}
