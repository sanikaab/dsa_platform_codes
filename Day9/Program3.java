/*Code 3:Buildings receiving sunlight
Company: Amazon, Microsoft
Given are the heights of certain Buildings which lie adjacent to each other. Sunlight
starts falling from the left side of the buildings. If there is a building of a certain Height,
all the buildings to the right side of it having lesser heights cannot see the sun. The task
is to find the total number of such buildings that receive sunlight.

Example 1:
Input:
N = 6
H[] = {6, 2, 8, 4, 11, 13}
Output:
4
Explanation:
Only buildings of height 6, 8, 11 and
13 can see the sun, hence output is 4.

Example 2:

Input:
N = 5
H[] = {2, 5, 1, 8, 3}
Output:
3
Explanation:
Only buildings of height 2, 5 and 8
can see the sun, hence output is 3.

Example 3:
Input:
N = 7
H[] = {3, 4, 1, 0, 6, 2, 3}
Output:
3
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1 ≤ N ≤ 105
1 ≤ Hi ≤ 105
*/

import java.util.*;

class ArrayDemo {

	int sunLight(int arr[]) {

		int cnt = 0;
		for(int i=1;i<arr.length;i++) {
			int j = i-1;
			if(arr[j] < arr[i]) {
				
				cnt ++;
			}
		}

		return (cnt+1);
	}
	
}

class Client {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter size :");
		int size = sc.nextInt();
		
		int arr[] = new int[size];

		System.out.println("Enter elements :");
		for(int i=0;i<arr.length;i++) {

			arr[i] = sc.nextInt();
		}

		int retVal = obj.sunLight(arr);
		System.out.println("Count :" + retVal);
	}
}
