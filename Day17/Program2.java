
/*
 Code 2: Kth smallest element
Company: VMWare, Accolite, Amazon, Microsoft, Snapdeal, Hike, Adobe,
Google, ABCO, Cisco
Platform: GFG
Bubbar’s DSA sheet.
Description:
Given an array arr[] and an integer K where K is smaller than size of array,
the task is to find the Kth smallest element in the given array. It is given
that all array elements are distinct.
*Note :- l and r denotes the starting and ending index of the array.
Example 1:
Input:
N = 6
arr[] = 7 10 4 3 20 15
K = 3
L=0 R=5
Output : 7
Explanation :
3rd smallest element in the given
array is 7.

Example 2:
Input:
N = 5
arr[] = 7 10 4 20 15
K = 4 L=0 R=4
Output : 15
Explanation :
4th smallest element in the given
array is 15.

Your Task:

You don't have to read input or print anything. Your task is to complete the
function kthSmallest() which takes the array arr[], integers l and r denoting
the starting and ending index of the array and an integer K as input and
returns the Kth smallest element.

Expected Time Complexity: O(n*log(n) )
Expected Auxiliary Space: O(log(n))
Constraints:
1 <= N <= 105
L==0
R==N-1
1<= arr[i] <= 105
1 <= K <= N
*/

import java.util.*;

class MergeSort {
	void merge(int arr[],int start,int mid,int end) {
		int n1 = mid - start + 1;
		int n2 = end - mid;
		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		for(int i = 0 ; i < n1 ; i++) {
			arr1[i] = arr[start + i];
		}

		for(int i = 0 ; i < n2 ; i++) {
			arr2[i] = arr[mid + 1 + i];
		}

		int i = 0,j = 0,k = start;

		while(i < arr1.length && j < arr2.length) {
			if(arr1[i] < arr2[j]) {
				arr[k] = arr1[i];
				i++;
			}else {
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < arr1.length) {
			arr[k] = arr1[i];
			i++;
			k++;
		}

		while(j < arr2.length) {
			arr[k] = arr2[j];
			j++;
			k++;
		}
	}
	void mergeSort(int arr[],int start,int end) {
		if(start < end) {
			int mid = (start + end) / 2;
			mergeSort(arr,start,mid);
			mergeSort(arr,mid + 1,end);
			merge(arr,start,mid,end);
		}
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the element of the array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		MergeSort obj = new MergeSort();
		int start = 0;
		int end = arr.length - 1;
		obj.mergeSort(arr,start,end);

		System.out.print("Enter the kth smallest number : ");
		int num = sc.nextInt();
		System.out.println("Kth smallest element is : " + arr[num - 1]);
	}
}
