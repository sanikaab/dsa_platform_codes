
/*
 Medium Level : Day17
 Code 1: Kth Largest Element in an Array
Company: Amazon, Microsoft, Walmart, Adobe
Platform: Leetcode - 215
Striver’s and love Bubbar’s DSA sheet.
Description:
Given an integer array nums and an integer k, return the kth largest
element in the array.
Note that it is the kth largest element in the sorted order, not the kth distinct
element.
Can you solve it without sorting?

Example 1:
Input: nums = [3,2,1,5,6,4], k = 2
Output: 5
Example 2:
Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
Output: 4

Constraints:
1 <= k <= nums.length <= 105
-104 <= nums[i] <= 104
*/

import java.util.*;

class MergeSort {
	void merge(int arr[],int start,int mid,int end) {
		int n1 = mid - start + 1;
		int n2 = end - mid;
		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		for(int i = 0 ; i < n1 ; i++) {
			arr1[i] = arr[start + i];
		}

		for(int i = 0 ; i < n2 ; i++) {
			arr2[i] = arr[mid + 1 + i];
		}

		int i = 0,j = 0,k = start;

		while(i < arr1.length && j < arr2.length) {
			if(arr1[i] < arr2[j]) {
				arr[k] = arr1[i];
				i++;
			}else {
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < arr1.length) {
			arr[k] = arr1[i];
			i++;
			k++;
		}

		while(j < arr2.length) {
			arr[k] = arr2[j];
			j++;
			k++;
		}
	}
	void mergeSort(int arr[],int start,int end) {
		if(start < end) {
			int mid = (start + end) / 2;
			mergeSort(arr,start,mid);
			mergeSort(arr,mid + 1 , end);
			merge(arr,start,mid,end);
		}
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of the array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter the elements of the first array : ");
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.print("Enter the element at " + i + " index : ");
			arr[i] = sc.nextInt();
		}
		System.out.println();

		int start = 0;
		int end = arr.length - 1;
		MergeSort obj = new MergeSort();
		obj.mergeSort(arr,start,end);

		System.out.print("Enter the kth largest num : ");
		int num = sc.nextInt();
		System.out.println(arr[arr.length - num]);
	}
}
